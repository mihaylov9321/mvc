<?php
define('DEBUG', true);
define('DEFAULT_CONTROLLER', 'UsersController');

define('SITE_TITLE', 'New NVC framework');
define('DEFAULT_LAYOUT', 'default');

define('DB_HOST', 'localhost');
define('DB_NAME', 'mvc');
define('DB_USER', 'root');
define('DB_PASSWORD', '');