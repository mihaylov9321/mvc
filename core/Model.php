<?php

class Model
{
    protected $_db, $_table, $_modelName, $_softDelete, $_columNames = [];

    public function __construct($table)
    {
        $this->_db = DB::getInstance();
        $this->_table = $table;
        $this->_setTableColumns();
        $this->_modelName = str_replace('', '', ucwords(str_replace('_', ' ', $this->_table)));

    }

    protected function _setTableColumns()
    {
        $columns = $this->get_columns();
        foreach ($columns as $column) {
            $columnName = $column-> Field;
            $this->_columNames[] = $column->Field;
            $this->{$columnName} = null;
        }
    }

    public function get_columns()
    {
        return $this->_db->get_columns($this->_table);
    }

    public function find($params = [])
    {
        $results = [];
        $resultsQuery = $this->_db->find($this->_table, $params);
        foreach ($resultsQuery as $result) {
            $obj = new $this->_modelName($this->_table);
            $obj->populateObjData($result);
            $results[] = $obj;
        }
        return $results;
    }
    protected function _softDeleteParams($params){
        if($this->_softDelete){
            if(array_key_exists('conditions',$params)){
                if(is_array($params['conditions'])){
                    $params['conditions'][] = "deleted != 1";
                } else {
                    $params['conditions'] .= " AND deleted != 1";
                }
            } else {
                $params['conditions'] = "deleted != 1";
            }
        }
        return $params;
    }

    public function findFirst($params = [])
    {
        $params = $this->_softDeleteParams($params);
        $resultQuery = $this->_db->findFirst($this->_table, $params);
        $result = new $this->_modelName($this->_table);
        if ($resultQuery) {
            $result->populateObjData($resultQuery);
        }
        return $result;
    }

    public function findById($id)
    {
        return $this->findFirst(['conditions'=>'id = ?', 'bind'=>[$id]]);
    }


    public function save()
    {
        $fields =[];
        foreach ($this->_columNames as $column) {
            $fields[$column] = $this->$column;
        }
            if (property_exists($this, 'id' && $this->id != '')) {
                return $this->update($this->_table, $fields);
            } else {
                return $this->insert($fields);
            }
    }

    public function insert(array $fields)
    {
        if (empty($fields)) return false;
        return  $this->_db->insert($this->_table, $fields);
    }

    public function update($id, array $fields)
    {
        if (empty($fields)) return false;
        return $this->_db->update($this->_table, $id, $fields);
    }

    public function delete($id)
    {
        if ($id == '' && $this->id == '') return false;
        if ($this->_softDelete) {
            return $this->update($id, ['deleted'=>1]);
        }

        return $this->_db->delete($this->_table, $id);
    }

    public function query($sql, $bind = [])
    {
        return $this->_db->query($sql, $bind);
    }

    public function data()
    {
        $data = new stdClass();
        foreach ($this->_columNames as $column) {
            $data->column = $this->$column;
        }
        return $data;
    }

    public function assign($params)
    {
        if (!empty($params)) {
            foreach ($params as  $key=>$val) {
                if (in_array($key, $this->_columNames)) {
                    $this->$key = sanitize($val);
                }
            }
            return true;
        }
        return false;
    }

    public function populateObjData($result)
    {
        foreach ($result as $key=>$val) {
            $this->$key = $val;
        }
    }

}













