<?php
class Controller extends Application
{
    protected $_controller, $_action;

    public $view;

    public function __construct($controller, $action)
    {
        parent::__construct();
        $this->_controller = $controller;
        $this->_action = $action;
        $this->view = new view();
    }

    public function load_model($model)
    {

        if (class_exists($model)) {
            return $this->{$model.'Model'} = new $model;
        } else {
            echo 'Model not exist';
        }

    }

}