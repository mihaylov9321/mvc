<?php
class Router
{
    public static function route($url)
    {

        $controller = isset($url[0]) && $url[0] != '' ? ucwords($url[0]).'Controller' : DEFAULT_CONTROLLER;
        array_shift( $url);



        //action
        $action = isset($url[0]) && $url[0] != '' ? ucwords($url[0]).'Action' : 'indexAction';
        array_shift($url);


        $queryParams = $url;

        if (class_exists($controller)) {
            $controller = new $controller($action, $queryParams);
            if (method_exists($controller, $action)) {
                $controller->$action($queryParams);
            } else {
                die('That method does not exists in the controller.');
            }
        } else {
            die('That' . $controller . 'does not exists.');
        }

    }
    public static function redirect($location)
    {
        if(!headers_sent()) {
            header('Location: '.$location);
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="'.$location.'";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url='.$location.'"/>';
            echo '</noscript>';exit;
        }
    }

}