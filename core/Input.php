<?php

class Input
{
    public static function sanitized($dirty)
    {
        return htmlentities($dirty, ENT_QUOTES, "UTF-8");
    }

    public static function generateToken(){
        $token = base64_encode(openssl_random_pseudo_bytes(32));
//        Session::set('csrf_token',$token);
        return $token;
    }

    public static function csrfInput(){
        return '<input type="hidden" name="csrf_token" id="csrf_token" value="'.self::generateToken().'" />';
    }

    public static function get($input)
    {
        if (isset($_POST[$input])) {
            return self::sanitized($_POST[$input]);
        } else if(isset($_GET[$input])) {
            return self::sanitized($_GET[$input]);

        }
    }


}