<?php


class UsersController extends Controller


{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->load_model('Users');
        $this->view->setLayout('default');
    }

    public function indexAction()
    {
        $departments = new Departments();
        $departments = $departments->findAllDepartments();
        $users = $this->UsersModel->findAllUsers();
        $this->view->render('users/index', ['users' => $users, 'departments' => $departments]);
    }

    public function addAction()
    {

        $user = new Users();
        $validation = new Validate();
        if ($_POST) {
            $user->assign($_POST);
            $validation->check($_POST, Users::$addValidation);
            if ($validation->passed()) {
                $user->save();
                Router::redirect('/users');
            }
        }
        $departments = new Departments();
        $departments = $departments->findAllDepartments();

        $this->view->displayErrors = $validation->display_errors();
        $this->view->render('users/add', ['departments' => $departments]);
    }

    public function detailsAction($id)
    {
        $user = $this->UsersModel->findById($id);
        foreach ($user as $item) {
            $department = new Departments();
            $department = $department->findById($item["department_id"]);
        }
        $this->view->render('users/details', ['user' => $user, 'department'=> $department]);
    }

    public function  deleteAction($id)
    {
        $this->UsersModel->delete($id["0"]);
        Router::redirect('/users');
    }
}















