<?php
class DepartmentsController extends Controller
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->load_model('Departments');
        $this->view->setLayout('default');
    }

    public function indexAction()
    {
        $departments = $this->DepartmentsModel->findAllDepartments();
        $this->view->render('departments/index', ['departments' => $departments]);
    }

    public function addAction()
    {

        $departments = new Departments();
        $validation = new Validate();
        if ($_POST) {
            $departments->assign($_POST);
            $validation->check($_POST, Departments::$addValidation);
            if ($validation->passed()) {
                $departments->save();
                Router::redirect('/departments');
            }
        }

        $this->view->displayErrors = $validation->display_errors();
        $this->view->render('departments/add', ['departments' => $departments]);
    }

    public function detailsAction($id)
    {
        $departments = $this->DepartmentsModel->findById($id);
        $this->view->render('departments/details', ['departments' => $departments]);
    }

    public function  deleteAction($id)
    {
        $this->DepartmentsModel->delete($id["0"]);
        Router::redirect('/departments');
    }
}