<?php

class Users extends Model
{
    public $id, $name, $phone, $email, $comment, $department_id;
    public function __construct($user='')
    {
        $table = 'users';
        parent::__construct($table);

    }
    public static $addValidation = [
        'name' => [
            'display' => 'name',
            'required' => true,
            'unique' => 'users',
            'min' => 3,
        ],
        'email' => [
            'display' => 'email',
            'required' => true,
            'min' => 5,
            'unique' => 'users',
            'valid_email' => true,
        ],
        'phone' => [
            'display' => 'phone',
            'required' => true,
            'is_numeric' => true,
            'min' => 7,
        ],
        'comment' => [
            'display' => 'comments',
            'required' => true,
            'min' => 5,
        ],
        'department_id' => [
            'display' => 'department_id',
            'required' => true,
        ],
    ];


    public function findAllByUserId($user_id, $params=[]){
        $conditions = [
            'conditions' => 'user_id = ?',
            'bind' => [$user_id]
        ];
        $conditions = array_merge($conditions,$params);
        return $this->find($conditions);
    }

    public function findById($id)
    {
        $sql = "SELECT * FROM users WHERE id={$id["0"]}";
        $userQ = $this->query($sql, [])->results();
        return json_decode(json_encode($userQ), true);
    }

    public function findByUsername($username) {
        return $this->findFirst(['conditions'=> "username = ?", 'bind'=>[$username]]);
    }

    public function findAllUsers()
    {
        $usersQ = $this->query("SELECT * FROM users", [])->results();
        return json_decode(json_encode($usersQ), true);
    }

}