<?php

class Departments extends Model
{
    public function __construct()
    {
        $table = 'departments';
        parent::__construct($table);
    }

    public function findAllDepartments()
    {
        $departmentsQ = $this->query("SELECT * FROM departments", [])->results();
        return json_decode(json_encode($departmentsQ), true);

    }

    public function findById($id)
    {
        $idToFind = is_string($id) ? $id : $id["0"];
        $sql = "SELECT * FROM departments WHERE id={$idToFind}";
        $departmentsQ = $this->query($sql, [])->results();
        return json_decode(json_encode($departmentsQ), true);
    }

    public static $addValidation = [
        'name' => [
            'display' => 'name',
            'required' => true,
            'unique' => 'departments',
            'min' => 5,
        ],
    ];
}


















