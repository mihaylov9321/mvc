<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $this->_siteTitle; ?></title>
    <link rel="stylesheet" href="/public/css/main.css">
    <link rel="stylesheet" href="/public/css/fonts.css">
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
</head>
<body>


<nav class="navbar navbar-expand-sm navbar-blue bg-dark">


    <div class="collapse navbar-collapse" id="navbarsExample03">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/users">Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/departments">Departments</a>
            </li>


        </ul>

    </div>
</nav>
<?php echo $this->_content; ?>
<script defer src="/public/fonts/fontawesome-free/js/brands.js"></script>
<script defer src="/public/fonts/fontawesome-free/js/solid.js"></script>
<script defer src="/public/fonts/fontawesome-free/js/fontawesome.js"></script>
<script src="/public/js/jQuery3-5-1.min.js"></script>
<script src="/public/js/popper1-16-1.min.js"></script>
<script src="/public/js/bootstrap4-6-0.min.js"></script>
<script src="/public/js/main.js"></script>
</body>
</html>













