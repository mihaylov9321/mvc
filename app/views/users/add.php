<div class="card col-8 mx-auto mt-5">
    <div class="card-header">
        <h1 class="text-center">Сотрудники</h1>
    </div>
    <form action="add" method="POST">
        <div class=""><?= $this->displayErrors; ?></div>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Добавить Сотрудника</h5>
        </div>
        <div class="modal-body">
            <div class="form-group col-lg-12 mb-2">
                <div class="col-xs-8">
                    <input type="text" name="name" class="form-control" id="name"
                           placeholder="Введите Имя">
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="form-group col-lg-12 mb-2">
                <div class="col-xs-8">
                    <input type="email" name="email" class="form-control" id="email"
                           placeholder="Введите email ">
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="form-group col-lg-12 mb-2">
                <div class="col-xs-8">
                    <input type="phone" name="phone" class="form-control" id="phone"
                           placeholder="Введите Номер телефона">
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="form-group col-lg-12 mb-2">
                <div class="col-xs-8">
                    <input type="comment" name="comment" class="form-control" id="comment"
                           placeholder="Введите Комментарий">
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="form-group col-lg-12 mb-2">
                <div class="col-xs-8">
                    <select name="department_id" class="form-control" id="department_id" style="height: 37px">
                        <option value="">Выберите Департамент</option>
                        <?php foreach ($data["departments"] as $department=>$val) { ?>

                        <option value="<?php echo $val["id"]; ?>"><?php echo $val["name"]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a type="button" class="btn btn-secondary" href="/users">Back</a>
            <button type="submit" class="btn btn-primary">Save</button>

        </div>
    </form>
</div>