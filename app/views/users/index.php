<div class="card col-8 mx-auto mt-5">
    <div class="card-header">
        <h1 class="text-center">Сотрудники</h1>
        <?php
        if (count($data['departments']) > 0) {
            ?>
            <a type="button" href="users/add" class="btn btn-success">
                <i class="fa fa-plus"></i> Добавить нового сотрудника
            </a>
            <?php
        } else {
            ?>
            <a type="button" href="departments/add" class="btn btn-success">
                <i class="fa fa-plus"></i> Сначала добавьте Департамент!
            </a>
            <?php
        }
        ?>
    </div>
    <div class="card-body">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Email</th>
                <th scope="col">Телефон</th>
                <th scope="col">Департамент</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data["users"] as $user => $item) { ?>
                <tr class="pointer">
                    <th scope="row"><?php echo $item["id"]; ?></th>
                    <td onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php echo $item["name"]; ?></td>
                    <td onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php echo $item["email"]; ?></td>
                    <td onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php echo $item["phone"]; ?></td>
                    <td onclick="location.href='users/details/<?php echo $item["id"]; ?>'"><?php

                        foreach ($data["departments"] as $department => $value) {
                            if ($item["department_id"] === $value['id']) {
                                echo $value["name"];
                            }
                        }
                         ?></td>
                    <td>
                        <a href="users/delete/<?php echo $item["id"]; ?>"
                           class="btn btn-danger btn-xs float-right" onclick="
                                if(!confirm('Do you really want to delete <?php echo $item["name"]; ?> ?')){
                                return false;}"><i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>

            </tbody>
        </table>

    </div>
</div>
