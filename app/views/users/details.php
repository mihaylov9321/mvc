<div class="container mt-5 text-center">
    <div class="row pt-5">
        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-body">
                    <?php
                    foreach ($data["user"] as $item) { ?>
                    <p><strong>Name: </strong><?php echo $item["name"]; ?></p>
                    <p><strong>Email: </strong><?php echo $item["email"]; ?></p>
                    <p><strong>Phone: </strong><?php echo $item["phone"]; ?></p>
                    <p><strong>Department: </strong><?php

                        foreach ($data["department"] as $value) {
                            echo $value["name"];
                        }
                        ?></p>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
