<h1>User Page</h1>
<div class="card col-8 mx-auto">
    <div class="card-header">
        <h1 class="text-center">Департаменты</h1>
    </div>
    <form action="add" method="POST">
        <div class=""><?= $this->displayErrors; ?></div>
        <div class="modal-header">
            <h5>Добавить департамент</h5>
        </div>
        <div class="modal-body">
            <div class="form-group col-lg-12 mb-2">
                <div class="col-xs-8">
                    <input type="text" name="name" class="form-control" id="name"
                           placeholder="Введите Имя Департамента">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a type="button" class="btn btn-secondary" href="/departments">Back</a>
            <button type="submit" class="btn btn-primary">Save</button>

        </div>
    </form>
</div>