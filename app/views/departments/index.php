<div class="card col-8 mx-auto mt-5">
    <div class="card-header">
        <h1 class="text-center">Департаменты</h1>
        <a href="departments/add" type="button" class="btn btn-success">
            <i class="fa fa-plus"></i> Добавить новый Департамент
        </a>
    </div>
    <div class="card-body">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Департаменты</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data["departments"] as $department=>$item) {?>
                <tr class="pointer">
                    <th scope="row"><?php echo $item["id"]; ?></th>
                    <td  onclick="location.href='departments/details/<?php echo $item["id"]; ?>'"><?php echo $item["name"]; ?></td>
                    <td>
                        <a href="departments/delete/<?php echo $item["id"]; ?>"
                           class="btn btn-danger btn-xs float-right" onclick="
                                if(!confirm('Do you really want to delete <?php echo $item["name"]; ?> ?')){
                                return false;}"><i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>

            </tbody>
        </table>

    </div>
</div>