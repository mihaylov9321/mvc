# mvc



## Getting started

To make it easy for you to get started with this MVC project, here's a list of recommended next steps.

##Create your DB in phpMyAdmin

- [ ] Tables are : 
- departments with columns "id"(AI,index), "name"(VARCHAR);
- users with columns "id"(AI), "name"(VARCHAR), "email"(VARCHAR), phone(INT), comment(VARCHAR), "department_id"(INDEX).
- [ ] Configs of DB you can find and change in file config.php. You need to define 'DB_HOST' ,'DB_NAME', 'DB_USER', 'DB_PASSWORD' . According to your settings of your DB.


## Routes of this project

- [ ] " /users " - List of users Page
- [ ] " /users/add " - Create new user Page
- [ ] " /users/details/{id} " - Details  user Page
- [ ] " /departments " - List of departments Page
- [ ] " /departments/add " - Create new department Page
- [ ] " /departments/details/{id} " - Details  department Page



***

# My contacts 
- [ ] Phone: +380 93 597 65 02
- [ ] Email : mihaylov9321@gmail.com
- [ ] LinkedIn : https://www.linkedin.com/in/illia-zharchynskyi-1122a7223/
- [ ] Telegram : @illich1

